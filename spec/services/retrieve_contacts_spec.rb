require 'rails_helper'

RSpec.describe "RetrieveContacts" do

  it "works!" do
    expect(RetrieveContacts.new("query")).to respond_to(:retrieve)
  end

  it "still works!" do
    expect(RetrieveContacts.new("query")).to respond_to(:retrieve_all)
  end

  describe "Retrieve partial" do
    ##
    # $ grep "Diam Sed Industries" exercise-data.json | sort | uniq | wc -l
    # 1
    it "one result for Diam Sed Industries" do
      q = "Diam Sed Industries"

      expect(RetrieveContacts.new(q).retrieve.length).to eq(1)
    end

    ##
    # $ grep name exercise-data.json | sort | uniq -d
    # "name": "Lance Young",
    it "two results for Lance Young" do
      q = "Lance Young"

      expect(RetrieveContacts.new(q).retrieve.length).to eq(2)
    end

    ##
    # $ grep "Amazing Spider Man" exercise-data.json| sort | uniq | wc -l
    # 0
    it "no results for Amazing Spider Man" do
      q = "Amazing Spider Man"

      expect(RetrieveContacts.new(q).retrieve.length).to eq(0)
    end

    ##
    # $ grep "Apple Systems" exercise-data.json | sort | uniq | wc -l
    # 2
    it "two results for working for evil empire" do
      q = "Apple Systems"

      expect(RetrieveContacts.new(q).retrieve.length).to eq(11)
    end
  end
end

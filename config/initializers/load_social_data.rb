require 'json'

##
# https://hackhands.com/ruby-read-json-file-hash/
module Searchsocialnetwork
  class Application < Rails::Application
    config.before_initialize do
      Rails.logger.info("Before Initialize: Load Social Data\n")
      file = File.read(Rails.root.join('data', 'exercise-data.json'))
      data_hash = JSON.parse(file)
      Rails.logger.info("Loaded items count #{data_hash.length}")

      Rails.application.config.social_data = data_hash
    end
  end
end

##
# app/services/retrieve_contacts.rb
class RetrieveContacts
  def initialize(query)
    @query = query
    @query = @query.downcase if @query
  end

  def retrieve
    # Return all for the time being
    Rails.application.config.social_data.select do |contact|
    contact['company'].downcase.include?(@query) ||
      contact['email'].downcase.include?(@query) ||
      contact['city'].downcase.include?(@query) ||
      contact['country'].downcase.include?(@query) ||
      contact['name'].downcase.include?(@query) ||
      contact['job_history'].any? { |e| e.downcase.include?(@query) }
    end
  end

  def retrieve_all
    Rails.application.config.social_data
  end
end
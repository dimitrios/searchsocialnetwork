class RetrieveController < ApplicationController
  before_action :set_default_response_format

  def all
    results = RetrieveContacts.new(nil).retrieve_all

    respond_to do |format|
      format.json { render json: results, status: :ok}
    end
  end

  def query
    results = RetrieveContacts.new(params[:queryvalue]).retrieve

    respond_to do |format|
      format.json { render json: results, status: :ok}
    end
  end

  private

    def set_default_response_format
      request.format = :json
    end
end

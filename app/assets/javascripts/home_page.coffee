#
# Grab and filter responses from main app
#

parseResponsesFn = (resp) ->
  # console.log("resp")
  # console.log(resp)
  htmlValue = ""
# Object { job_history: Array[2],
  for contact in resp
    console.log(contact)
    contactHtml = """
        <p class="large">
          <h2>#{contact['name']}</h2>
          <strong>Email</strong>: <a href="mailto:#{contact['email']}">#{contact['email']}</a><br>
          <strong>City</strong>: #{contact['city']}<br>
          <strong>Country</strong>: #{contact['country']}<br>
          <strong>Company</strong>: #{contact['company']}<br>
          <strong>Job History</strong>:<br>
          <ul>
      """

    for job in contact['job_history']
      contactHtml += "<li>#{job}"

    contactHtml += """</ul>
        </p>
      """
    htmlValue += contactHtml

  $('#responseContainer').html(htmlValue)

parseErrorFn = (error) ->
  console.error(error)
  htmlValue = "There was an error in retrieval"

  $('#responseContainer').html(htmlValue)

clickFn = (event) ->
  console.log("Click")
  queryValue = $("#socialNetworkForm input[type=text]").val()

  if queryValue.length == 0
    queryLocation = "/retrieve/all.json"
  else
    queryLocation = "/retrieve/query/#{queryValue}.json"

  console.log("Location: #{queryLocation}")

  $('#responseContainer').html("Please wait")
  jQuery.ajax({
    url: queryLocation,
    data: {},
    success: parseResponsesFn,
    error: parseErrorFn
  })
  return false

$(window).load ->
  $("#socialNetworkForm").on("click", clickFn)
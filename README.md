# README

## Deployed Location

<https://searchsocialnetwork.herokuapp.com/>

## Source Code

<https://gitlab.com/dimitrios/searchsocialnetwork>

## Problem Specification

> Attached is a JSON file containing someone's social network contacts.
  We want you to write a simple web app that allows a user to search these
  contacts. It should present the user with a search box and a list of all
  the people who match the current search. When there is no search term it
  should display all the contacts. The look of the application is unimportant -
  an input box and a list of the results is all we require.

## Exploring Dataset from Command Line

```bash
⋊> ~/p/explore_dataset grep name exercise-data.json | wc -l
100
⋊> ~/p/explore_dataset grep name exercise-data.json | sort | uniq | wc -l
99
⋊> ~/p/explore_dataset grep email exercise-data.json | wc -l
100
⋊> ~/p/explore_dataset grep email exercise-data.json | sort | uniq | wc -l
100
```

Not "name" as a unique key and dataset small

## Solution's Rationale

I considered two approaches

![Approaches](/approaches.jpg "Approaches")

First approach: Consume data to an RDBMS system or something similar and
retrieve from there.

Was the more "conservative" approach of the two, essentially create a mini-CRUD
application. Whenever users query something, do a ```SELECT``` statement under
the hood.

Second approach: "Data in Memory". Since the amount of data is small it makes
sense to somehow load them "in-memory" every time that the application starts
and filter them from there. Every query becomes an array-filter action under
the hood. I have seen this approach in many coding puzzles which seem to be the
bread and butter of contemporary IT recruitment workflow, such as
*"create a server that returns any of the first 1000 Fibonacci numbers 1000ms"*
where you pre-calculate the first 1000 numbers before the first request and then
serve them. This approach makes lots of sense in a SOA/micro-services
environment where one service might produce some data for others to use so when
they want something very specific (like a threshold value), they go there to
retrieve it.

I honestly hope that this approach does not alienate people.

## TODOs

- Ruby version to Gemfile (Heroku recommendation)
- Procfile (Heroku recommendation)
- Yarn (Heroku recommendation)
- Implement in parallel an RDBMS solution
